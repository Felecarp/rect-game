""" run a TCP server """
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet import reactor, task
from rects.core import RectManager
from rects.network import PORT, RectRemoteFactory
from rects.views import LogView


def run_TCP_server():
    """ run a TPC server """
    # pylint: disable=invalid-name
    manager = RectManager()
    endpoint = TCP4ServerEndpoint(reactor, PORT)
    # TCP controller
    endpoint.listen(RectRemoteFactory(manager))
    # log view
    view = LogView(manager)
    display_loop = task.LoopingCall(view.update)
    display_loop.start(1)
    # pylint: disable=no-member
    reactor.run()


if __name__ == "__main__":
    run_TCP_server()
