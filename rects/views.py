""" rects.views: View part of Rects Game """
import sys
from datetime import datetime
import pygame
import pygame.display
from .core import RectManagerListener, RectManager, MoveManager


SCREEN_SIZE = 500, 500
BACKGROUND_COLOR = "white"
RECT_COLOR = "red"

FPS = 20
SPEED = 64 / FPS


class LogView(RectManagerListener):
    """ write manager events in file """
    def __init__(self, manager: RectManager, file=sys.stdout):
        super().__init__(manager)
        self._file = file

    def _log(self, msg):
        self._file.write(datetime.now().strftime("[%x-%X]: ") + msg + "\n")

    def on_rect_created(self, rect_id):
        rect = self._manager.rects[rect_id]
        self._log(f"create rect#{rect_id} - {rect}")

    def on_rect_removed(self, rect_id):
        self._log(f"remove rect#{rect_id}")

    def on_rect_moved(self, rect_id):
        mem_rect = self._mem_rects[rect_id]
        rect = self._manager.rects[rect_id]
        move_x, move_y = rect.x-mem_rect.x, rect.y-mem_rect.y
        self._log(f"move rect#{rect_id} to {move_x};{move_y} - {rect}")


class PygameView(RectManagerListener):
    """ draw rect events in pygame screen """
    def __init__(self, manager: RectManager, move_manager: MoveManager):
        super().__init__(manager)
        self._move_manager = move_manager
        self.surface = None
        self.__init_display()
        self._dirty_rects = []

    def __init_display(self):
        print("init display")
        pygame.display.init()
        self.surface = pygame.display.set_mode(SCREEN_SIZE)
        self.surface.fill(BACKGROUND_COLOR)
        pygame.display.update()

    def _erase_rect(self, rect_id):
        mem_rect = self._mem_rects[rect_id]
        self._dirty_rects.append(self.surface.fill(
            BACKGROUND_COLOR, mem_rect))
        for b_rect_id, b_rect in self._manager.rects.items():
            if b_rect_id != rect_id and mem_rect.colliderect(b_rect):
                self.surface.fill(RECT_COLOR, b_rect)

    def _draw_rect(self, rect_id):
        rect = self._manager.rects[rect_id]
        self._dirty_rects.append(self.surface.fill(
            RECT_COLOR, rect))

    def on_rect_created(self, rect_id):
        self._draw_rect(rect_id)

    def on_rect_removed(self, rect_id):
        self._erase_rect(rect_id)

    def on_rect_moved(self, rect_id):
        self._erase_rect(rect_id)
        self._draw_rect(rect_id)

    def update_before(self):
        self._update_local_events()
        self._update_keys()

    def _update_keys(self):
        # pylint: disable=no-member
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_LEFT]:
            self._move_manager.move_rects(-SPEED, 0)
        if pressed[pygame.K_UP]:
            self._move_manager.move_rects(0, -SPEED)
        if pressed[pygame.K_RIGHT]:
            self._move_manager.move_rects(SPEED, 0)
        if pressed[pygame.K_DOWN]:
            self._move_manager.move_rects(0, SPEED)

    def _update_local_events(self):
        # pylint: disable=no-member
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.active = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_r:
                    self._move_manager.create_rect()
                elif event.key == pygame.K_ESCAPE:
                    self.active = False

    def update_after(self):
        pygame.display.update(self._dirty_rects)
        self._dirty_rects.clear()
