# Rect game

A simple rect moving game in Python3. It was developped to test some features of the Twisted framework.

## Installation

1. install interpreter and package managers: `sudo apt install git python3 python3-pip`
1. download repository: `git clone https://gitlab.com/felecarp/rect-game` or `git clone git@gitlab.com:Felecarp/rect-game.git` (prefer ssh if you have an account on gitlab.com)
1. install dependencies: `python3 -m pip install -r requirements.txt`

## Execution

1. launch a server: `python3 runserver.py`
1. launch a client: `python3 runclient.py`

You can edit the HOST constant in "rects/client.py"

## Play

To move your rect, press directional keys. 

## Test

To run test suite: `python3 -m unittest`