""" rects.network: Controller part of Rects Game """
from twisted.internet import task
from twisted.internet.protocol import Factory
from twisted.protocols.amp import Command, Integer, AMP
from .core import RectManager, RectManagerListener,\
                  MoveManager, MoveManagerListener


HOST = "localhost"
PORT = 4444


class MoveRect(Command):
    """ move an existing rect - Client Action """
    arguments = [
        (b"rect_id", Integer()),
        (b"move_x", Integer()),
        (b"move_y", Integer()),
    ]


class CreateRect(Command):
    """ create a new rect - Client Action """
    response = [
        (b"rect_id", Integer()),
    ]


class RemoveRect(Command):
    """ remove an existing rect - Server Event """
    arguments = [
        (b"rect_id", Integer()),
    ]


class RectEvent(Command):
    """ move an existing rect - Server Event """
    arguments = [
        (b"rect_id", Integer()),
        (b"pos_x", Integer()),
        (b"pos_y", Integer()),
    ]


class RemotedRectManagerListener(RectManagerListener):
    """ Call remote when rect event """
    def __init__(self, manager: RectManager, remote):
        super().__init__(manager)
        self._remote = remote

    def on_rect_created(self, rect_id):
        self._remote.call_rect(rect_id)

    def on_rect_moved(self, rect_id):
        self._remote.call_rect(rect_id)

    def on_rect_removed(self, rect_id):
        self._remote.call_remove_rect(rect_id)


class ClientRectRemote(AMP):
    """ send RealRectManager events and process actions """
    # pylint: disable=too-many-ancestors
    def __init__(self, manager: RectManager, boxReceiver=None, locator=None):
        super().__init__(boxReceiver=boxReceiver, locator=locator)
        self._manager = manager
        self._listener = RemotedRectManagerListener(self._manager, self)
        self._rect_ids = set()
        self._event_loop = None

    def connectionMade(self):
        self._event_loop = task.LoopingCall(self._listener.update)
        self._event_loop.start(0.01)

    def connectionLost(self, reason):
        self._event_loop.stop()
        for rect_id in self._rect_ids:
            self._manager.remove_rect(rect_id)

    @CreateRect.responder
    def create_rect(self, rect_id=None):
        """ create a rect in manager and return id """
        rect_id = self._manager.create_rect()
        self._rect_ids.add(rect_id)
        return {"rect_id": rect_id}

    @MoveRect.responder
    def move_rect(self, rect_id: int, move_x: int, move_y: int):
        """ move a rect in manager """
        if rect_id not in self._rect_ids:
            raise ValueError("Unauthorized to move rect %i" % rect_id)
        self._manager.move_rect(rect_id, move_x, move_y)
        return {}

    def call_rect(self, rect_id):
        """ client make rect """
        rect = self._manager.rects[rect_id]
        self.callRemote(RectEvent, rect_id=rect_id, pos_x=rect.x, pos_y=rect.y)

    def call_remove_rect(self, rect_id):
        """ client remove rect """
        self.callRemote(RemoveRect, rect_id=rect_id)


class RectRemoteFactory(Factory):
    """ build ClientRectRemote """
    def __init__(self, manager: RectManager):
        self._manager = manager

    def buildProtocol(self, addr) -> ClientRectRemote:
        return ClientRectRemote(self._manager)


class RemotedMoveManagerListener(MoveManagerListener):
    """ send actions to server by remote """
    def __init__(self, manager: MoveManager, rect_manager: RectManager, remote):
        super().__init__(manager)
        self._remote = remote
        self._rect_manager = rect_manager

    def move_rect(self, rect_id, move):
        move_x, move_y = move
        self._remote.callRemote(
            MoveRect, rect_id=rect_id, move_x=move_x, move_y=move_y)

    def create_rect(self):
        creating = self._remote.callRemote(CreateRect)
        def register(result):
            rect_id = result["rect_id"]
            self._manager.rect_ids.add(rect_id)
        creating.addCallback(register)


class ServerRectRemote(AMP):
    """ Send moves to a server """
    # pylint: disable=too-many-ancestors
    def __init__(self, manager: RectManager, move_manager: MoveManager,
                 boxReceiver=None, locator=None):
        super().__init__(boxReceiver=boxReceiver, locator=locator)
        self._manager = manager
        self._listener = RemotedMoveManagerListener(move_manager, manager, self)
        self._action_loop = None

    def connectionMade(self):
        self._action_loop = task.LoopingCall(self._listener.update)
        self._action_loop.start(0.01)

    def connectionLost(self, reason):
        self._action_loop.stop()
        print(f"connection lost: {reason}")

    @RectEvent.responder
    def rect(self, rect_id, pos_x, pos_y):
        """ place a rect in manager """
        self._manager.rect(rect_id, pos_x, pos_y)
        return {}

    @RemoveRect.responder
    def remove_rect(self, rect_id):
        """ remove a rect in manager """
        self._manager.remove_rect(rect_id)
        return {}
