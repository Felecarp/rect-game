""" Test rects.core """
import unittest
from pygame import Rect
from rects.core import RectManager, RectManagerListener,\
                       MoveManager, MoveManagerListener


class RectManagerTestCase(unittest.TestCase):
    """ test rects.core.RectManager """
    def setUp(self):
        self.manager = RectManager()

    def test_create_rect(self):
        """ test RectManager.create_rect """
        rect_id = self.manager.create_rect()
        self.assertEqual(rect_id, 1)
        self.assertEqual(self.manager.rects, {1: Rect(0, 0, 64, 64)})
        rect_id = self.manager.create_rect()
        self.assertEqual(rect_id, 2)
        self.assertEqual(self.manager.rects, {1: Rect(0, 0, 64, 64), 2: Rect(0, 0, 64, 64)})

    def test_remove_rect(self):
        """ test RectManager.remove_rect """
        rect_id = self.manager.create_rect()
        self.assertEqual(self.manager.rects, {1: Rect(0, 0, 64, 64)})
        self.manager.remove_rect(rect_id)
        self.assertEqual(self.manager.rects, {})

    def test_move_rect(self):
        """ test RectManager.move_rect """
        rect_id = self.manager.create_rect()
        self.manager.move_rect(rect_id, 5, -20)
        rect = self.manager.rects[rect_id]
        self.assertEqual(rect.topleft, (5, 0))

    def test_rect(self):
        """ test RectManager.rect """
        rect_id = self.manager.create_rect()
        self.manager.rect(rect_id, 15, -5)
        rect = self.manager.rects[rect_id]
        self.assertEqual(rect.topleft, (15, -5))


class MoveManagerTestCase(unittest.TestCase):
    """ test rects.core.MoveManager """
    def setUp(self):
        self.manager = MoveManager()

    def test_move_rects(self):
        """ test MoveManager.move_rects """
        self.manager.rect_ids.add(9)
        self.manager.move_rects(9, -6)
        self.manager.rect_ids.add(5)
        self.manager.move_rects(-17, 9)
        moves = set((rect_id, tuple(move))
                    for rect_id, move in self.manager.clear_moves())
        self.assertEqual(moves, {(9, (-8, 3)), (5, (-17, 9))})


class RectManagerListenerTestCase(unittest.TestCase):
    """ test rects.core.RectManagerListener """
    def setUp(self):
        self.manager = RectManager()
        self.listener = RectManagerListener(self.manager)
        self.created = []
        self.removed = []
        self.moved = []
        self.listener.on_rect_created = self.created.append
        self.listener.on_rect_removed = self.removed.append
        self.listener.on_rect_moved = self.moved.append

    def test_on_rect_created(self):
        """ test RectManagerListener.on_rect_created """
        rect_id = self.manager.create_rect()
        self.listener.update()
        self.assertEqual(self.created, [rect_id])

    def test_on_rect_removed(self):
        """ test RectManagerListener.on_rect_removed """
        rect_id = self.manager.create_rect()
        self.listener.update()
        self.manager.remove_rect(rect_id)
        self.listener.update()
        self.assertEqual(self.removed, [rect_id])

    def test_on_moved(self):
        """ test RectManagerListener.on_rect_moved """
        rect_id = self.manager.create_rect()
        self.listener.update()
        self.manager.move_rect(rect_id, 9, 8)
        self.listener.update()
        self.assertEqual(self.moved, [rect_id])


class MoveManagerListenerTestCase(unittest.TestCase):
    """ test rects.core.MoveManagerListener """
    def setUp(self):
        self.manager = MoveManager()
        self.listener = MoveManagerListener(self.manager)
        self.moves = []
        def register_move(rect_id, move):
            self.moves.append((rect_id, tuple(move)))
        self.listener.move_rect = register_move

    def test_move_rect(self):
        """ test MoveManagerListener.move_rect """
        self.manager.rect_ids.add(9)
        self.manager.move_rects(8, 6)
        self.listener.update()
        self.assertEqual(self.moves, [(9, (8, 6))])
