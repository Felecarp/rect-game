""" run a TCP/GUI client """
from twisted.internet.endpoints import TCP4ClientEndpoint, connectProtocol
from twisted.internet import reactor, task
from rects.core import RectManager, MoveManager
from rects.network import ServerRectRemote, HOST, PORT
from rects.views import PygameView, FPS


def run_TCP_GUI_client():
    """ run a TCP/GUI client """
    # pylint: disable=invalid-name
    manager = RectManager()
    move_manager = MoveManager()
    move_manager.create_rect()
    # TCP controller
    proto = ServerRectRemote(manager, move_manager)
    endpoint = TCP4ClientEndpoint(reactor, HOST, PORT)
    connectProtocol(endpoint, proto)
    # pygame view
    view = PygameView(manager, move_manager)
    display_loop = task.LoopingCall(view.update)
    display_loop.start(1/FPS)
    # stopper
    def stopper():
        # pylint: disable=no-member
        if not view.active:
            reactor.stop()
    stop_loop = task.LoopingCall(stopper)
    stop_loop.start(1/FPS)
    # pylint: disable=no-member
    reactor.run()


if __name__ == "__main__":
    run_TCP_GUI_client()
