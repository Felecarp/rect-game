""" rects.core: Model part of Rects Game """
from pygame import Rect


class RectManager:
    """ manage real rects

    used by servers """
    def __init__(self):
        self.rects = {}
        self.__rect_counter = 0

    def _count_rect(self):
        self.__rect_counter += 1
        return self.__rect_counter

    def create_rect(self) -> int:
        """ create a new rect - return id """
        rect = Rect(0, 0, 64, 64)
        rect_id = self._count_rect()
        assert isinstance(rect_id, int)
        self.rects[rect_id] = rect
        return rect_id

    def remove_rect(self, rect_id):
        """ remove a rect """
        del self.rects[rect_id]

    def move_rect(self, rect_id, move_x, move_y):
        """ move a rect """
        rect = self.rects[rect_id]
        rect.move_ip(move_x, move_y)
        if rect.x < 0:
            rect.x = 0
        if rect.y < 0:
            rect.y = 0

    def rect(self, rect_id, pos_x, pos_y):
        """ create a new rect """
        if rect_id in self.rects:
            rect = self.rects[rect_id]
        else:
            rect = Rect(0, 0, 64, 64)
        rect.topleft = (pos_x, pos_y)
        self.rects[rect_id] = rect


class MoveManager:
    """ manage movements of rects """
    def __init__(self):
        self.rect_ids = set()
        self._moves = {}
        self._creating_rects = 0

    def create_rect(self):
        """ create a new rect """
        self._creating_rects += 1

    def move_rects(self, move_x, move_y):
        """ move all my rects """
        for rect_id in self.rect_ids:
            self._move_rect(rect_id, move_x, move_y)

    def _move_rect(self, rect_id, move_x, move_y):
        """ move rect action """
        if rect_id in self._moves:
            o_move_x, o_move_y = self._moves[rect_id]
            self._moves[rect_id] = o_move_x+move_x, o_move_y+move_y
        else:
            self._moves[rect_id] = (move_x, move_y)

    def clear_moves(self):
        """ iter move actions """
        for rect_id, (move_x, move_y) in self._moves.items():
            yield rect_id, (int(move_x), int(move_y))
            self._moves[rect_id] = (move_x-int(move_x), move_y-int(move_y))

    def clear_creating_rects(self) -> int:
        """ get creating rects """
        creating_rects = self._creating_rects
        self._creating_rects = 0
        return creating_rects


class RectManagerListener:
    """ react on rect manager events """
    def __init__(self, manager: RectManager):
        self._manager = manager
        self._mem_rects = {}
        self.active = True

    def _memorize(self, rect_id):
        assert rect_id in self._manager.rects
        self._mem_rects[rect_id] = self._manager.rects[rect_id].copy()

    def update(self):
        """ check manager status """
        self.update_before()
        if not self.active:
            return
        # check memorized rects are here
        new_mem_rects = self._mem_rects.copy()
        for rect_id in self._mem_rects:
            if not rect_id in self._manager.rects:
                self.on_rect_removed(rect_id)
                del new_mem_rects[rect_id]
        self._mem_rects = new_mem_rects
        # check present rects are the same
        for rect_id, rect in self._manager.rects.items():
            if rect_id in self._mem_rects:
                mem_rect = self._mem_rects[rect_id]
                if mem_rect != rect:
                    self.on_rect_moved(rect_id)
                    self._memorize(rect_id)
            else:
                self.on_rect_created(rect_id)
                self._memorize(rect_id)
        self.update_after()

    def on_rect_created(self, rect_id: int):
        """ called when rect is created """

    def on_rect_removed(self, rect_id: int):
        """ called when rect is removed """

    def on_rect_moved(self, rect_id: int):
        """ called when rect is moved """

    def update_before(self):
        """ called before update """

    def update_after(self):
        """ called after update """


class MoveManagerListener:
    """ react on action events """
    def __init__(self, manager: MoveManager):
        self._manager = manager
        self.active = True

    def update(self):
        """ check manager events """
        if self.active:
            for rect_id, move in self._manager.clear_moves():
                self.move_rect(rect_id, move)
            # pylint: disable=unused-variable
            for i in range(self._manager.clear_creating_rects()):
                self.create_rect()

    def move_rect(self, rect_id, move):
        """ called to move a rect """

    def create_rect(self):
        """ called to create a new rect """
